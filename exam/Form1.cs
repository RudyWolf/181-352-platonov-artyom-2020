﻿using SemesterProject.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemesterProject
{
    public partial class Form1 : Form
    {
        const int mapWidth = 30;
        const int mapHight = 30;
        const int sizeCell = 15;
        public int lvlSpeed = 80;
        public int lvlNum = 1;
        public int[,] resMap = new int[mapHight, mapWidth];
        public int[,] map = new int[mapHight, mapWidth];
        public int dirX = 0;
        public int dirY = 0;
        public int platformX;
        public int platformY;
        public int ballX;
        public int ballY;
        public int score;
        public bool level = false;
        public int intLevel = 0;

        public Image arcanoidSet;
        public Image arcanoidSet2;
        public Label scoreLabel;
        public Label lvlLabel;
      
        
        public Form1()
        {
            InitializeComponent();
            timer1.Tick += new EventHandler(update);
            //lvl.Tick += new EventHandler(addLvl);
            this.KeyDown += new KeyEventHandler(inputCheck);
            scoreLabel = new Label();
            lvlLabel = new Label();
            scoreLabel.Location = new Point(sizeCell * 12, (mapHight * sizeCell) + sizeCell);
            lvlLabel.Location = new Point(sizeCell * 12, (mapHight * sizeCell) + sizeCell +30);
            scoreLabel.Text = "Score: " + score;
            lvlLabel.Text = "LVL: " + lvlNum;
            
        }

        private void inputCheck(object sender, KeyEventArgs e) //Управление платформой
        {
            map[platformY, platformX] = 0;
            map[platformY, platformX + 1] = 0;
            switch (e.KeyCode)
            {                
                case Keys.Right:
                    if (platformX+2 < mapWidth-1)
                    {
                        platformX++;
                    }
                    break;
                case Keys.Left:
                    if (platformX > 0)
                    {
                        platformX--;
                    }
                    break;
                    
            }
            map[platformY, platformX] = 9;
            map[platformY, platformX + 1] = 99;
            map[platformY, platformX + 2] = 999;
        }


        private void addLvl() {
            lvlSpeed -= 10;
            lvlNum++;
            lvlLabel.Text = "LVL: " + lvlNum;
            if (lvlSpeed > 0)
            {
                timer1.Interval = lvlSpeed;
            }
            else {
                timer1.Interval = 1; 
            }
            

        }
        private void update(object sender, EventArgs e) // проверка проигрыша, вызов проверки коллизий
        {
            if (ballY + dirY > mapHight - 1) {

                endGame();
            }
            map[ballY, ballX] = 0;
            if (!isCollide())
                ballX += dirX;
            if (!isCollide())
                ballY += dirY;
            map[ballY, ballX] = 8;
            Invalidate();
        }

        public void generatePlatforms() //генерация платформ
        {
            Random r = new Random();
            for (int i = 0; i < mapHight/3; i++) {
                for (int j = 0; j < mapWidth; j+=2) {
                    int currPlat =  r.Next(0,7);
                    map[i, j] = currPlat;
                    map[i, j + 1] = currPlat + currPlat*10;
                }
            }
        }
        public bool isCollide() //сверка коллизий, присвоение очков
        {
            bool collide = false;
            if (ballX + dirX > mapWidth - 1 || ballX + dirX < 0) { // коллизия со стенками
                dirX *= -1;
                collide =  true;
            }
            if (ballY + dirY < 0) //коллизия с потолком
            {
                dirY *= -1;
                collide =  true;
            }
            if (map[ballY, ballX + dirX] != 0) {  //коллизия с объектами
                if (map[ballY, ballX + dirX] > 10 && map[ballY, ballX + dirX] != 99 && map[ballY, ballX + dirX] != 999)
                {
                    map[ballY, ballX + dirX] = 0;
                    map[ballY, ballX + dirX-1] = 0;
                    score += 50;
                }
                else if (map[ballY + dirY, ballX] < 9) {
                    map[ballY, ballX + dirX] = 0;
                    map[ballY, ballX + dirX + 1] = 0;
                    score += 50;
                }
                dirX *= -1;
                collide = true;
            }
            scoreLabel.Text = "Score: " + score;
            if (map[ballY + dirY, ballX] != 0)
            {
                if (map[ballY + dirY, ballX] > 10 && map[ballY + dirY, ballX] != 99)
                {
                    map[ballY + dirY, ballX] = 0;
                    map[ballY + dirY, ballX-1] = 0;
                    score += 50;
                }
                else if (map[ballY + dirY, ballX] < 9)
                {
                    map[ballY + dirY, ballX] = 0;
                    map[ballY + dirY, ballX + 1] = 0;
                    score += 50;
                }
                if (score % 200 == 0) {
                    addLvl();
              }
                if (score % 1000 == 0)
                {
                    AddLine();
                }
                dirY *= -1;
                collide = true;
            }
            return collide;
        }
        public void init() //инициализация основных элементов. 
        {
            this.Width = mapWidth * sizeCell +18;
            this.Height = mapHight * sizeCell +sizeCell*10;
            lvlNum = 1;
            lvlSpeed = 90;
            score = 0;

            arcanoidSet = new Bitmap(Resources.arcanoid); // l
            arcanoidSet2 = new Bitmap(Resources.arcanoid2);
            scoreLabel.Text = "Score: "+score;
            lvlLabel.Text = "LVL: " + lvlNum;
            this.Controls.Add(scoreLabel);
            this.Controls.Add(lvlLabel);
            timer1.Interval = lvlSpeed;
            

            platformX = (mapWidth-3)/2;
            platformY = mapHight-1;

            ballX = platformX + 1;
            ballY = platformY - 1;

            for (int i = 0; i < mapHight; i++) {
                for (int j = 0; j < mapWidth; j++) {
                    map[i, j] = resMap[j,i];
                }
            }
            map[platformY, platformX] = 9;
            map[platformY, platformX + 1] = 99;
            map[platformY, platformX + 2] = 999;

            map[ballY, ballX] = 8;
            dirX = 1;
            dirY = -1;

            if (!level) { generatePlatforms(); };
            DialogResult result = MessageBox.Show("Начать?", "????", MessageBoxButtons.YesNo);

            if (DialogResult.Yes == result)
            {
                timer1.Start();
            }
            
            
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void drawMap(Graphics g) //отрисовка игровых элементов
        {

            for (int i = 0; i < mapHight; i++)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    if (map[i, j] == 9)
                    {
                        g.DrawImage(arcanoidSet, new Rectangle(new Point(j*sizeCell, i*sizeCell), new Size(sizeCell*3, sizeCell*1)), 398, 17, 150, 50, GraphicsUnit.Pixel); //числа платформы
                    }
                    if (map[i, j] == 8) {
                        g.DrawImage(arcanoidSet, new Rectangle(new Point(j * sizeCell, i * sizeCell), new Size(sizeCell * 1, sizeCell * 1)), 806, 548, 73, 73, GraphicsUnit.Pixel); //числа мяча
                    }
                   
                    if (map[i, j] > 0 && map[i, j] < 8)
                    {
                        g.DrawImage(arcanoidSet, new Rectangle(new Point(j * sizeCell, i * sizeCell), new Size(sizeCell * 2, sizeCell * 1)), 20, 16+77*(map[i,j]-1), 170, 59, GraphicsUnit.Pixel); //числа мяча
                    }
                   
                }
            }

        }
        private void paint(object sender, PaintEventArgs e) 
        {
            drawMap(e.Graphics);
            drawArea(e.Graphics);
        }

        public void AddLine() //добавление новой линии 
        {

            for (int i = mapHight - 2; i > 0; i--)
            {
                for (int j = 0; j < mapWidth; j++)
                {

                    if (map[i, j] != 8)
                    {
                        map[i, j] = map[i - 1, j];
                    }
                }
            }
        
            Random r = new Random(); 
            for (int j = 0; j < mapWidth; j += 2)
            {
                int currPlat = r.Next(0, 7);
                map[0, j] = currPlat;
                map[0, j + 1] = currPlat + currPlat * 10;
            }
            for (int j = 0; j < mapWidth; j += 2) {
                if (map[mapHight - 2, j] != 0 && map[mapHight - 2, j] != 9 && map[mapHight - 2, j] != 99 && map[mapHight - 2, j] != 999 && map[mapHight - 2, j] != 8) {
                    endGame();
                }
            }

                Invalidate();
            }
        public void drawArea(Graphics g) //отрисовка границ игрового поля
        { 
        g.DrawRectangle(Pens.Black, new Rectangle(0, 0, mapWidth*sizeCell, mapHight * sizeCell)); 
        }
        public void endGame() {
            Form2 tableResult = new Form2(score, intLevel);
            
            timer1.Stop();
            lvl.Stop();
            
            tableResult.ShowDialog();
            if (DialogResult.OK == tableResult.DialogResult)
            {
                DialogResult result = MessageBox.Show("Проверьте топ 10 игроков", "это успех", MessageBoxButtons.OK);

                init();
            }
            else {
                DialogResult result = MessageBox.Show("Ваши результаты не были внесены в топ","(", MessageBoxButtons.OK);
                init();
            } 
        }

        private void loadFm(object sender, EventArgs e)
        {
            init();
        }
    }
}
