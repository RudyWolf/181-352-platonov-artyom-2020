﻿using SemesterProject.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemesterProject
{
    public partial class redactor : Form
    {
        private Bitmap boardBitm;
        public bool isPressed = false;
        private Dictionary<char, Rectangle> pics = new Dictionary<char, Rectangle>(); //координаты вырезания объекта
        private Dictionary<int, char> bind = new Dictionary<int, char>(); // координаты вырезания
        char[] ims = { '6' };
        string path = @".\dict.txt";
        public int Rows { get; private set; } = 10;
        Image img = Resources.block;
        public int Cols { get; private set; } = 30;
        public int xCW;//ширина ячейки
        public int xCH; // высота ячейки
        public int selected = 0;
        public Dictionary<int[], char> dict = new Dictionary<int[], char>();  //хранит уже сделанное на карте
        private Point StartPoint;
        public redactor()
        {
            InitializeComponent();
            DictFill();
            DrawCells();
            button1.Click += Bsave_Click;
            pboard.Paint += Pboard_Paint;
            pboard.MouseUp += Pboard_MouseUp;

            pboard.MouseMove += Pboard_MouseMove;
            pboard.MouseDown += Pboard_MouseDown;
        }
        private void Pboard_MouseDown(object sender, MouseEventArgs e) //нажатие мышки
        {
            StartPoint = new Point(e.X, e.Y);
            isPressed = true;
        }

        private void Pboard_MouseMove(object sender, MouseEventArgs e) //передвижение мышки
        {
            if (e.Button == MouseButtons.Right)
            {
                Point x = new Point(Cursor.Position.X - StartPoint.X, Cursor.Position.Y - StartPoint.Y);
                if (sender is Control)
                {
                    ((Control)sender).Location = PointToClient(x);
                }
            }
            if (e.Button == MouseButtons.Left)
            {
                if (isPressed)
                {
                    Pboard_MouseUp(sender, e);
                };
            }
        }

        private void Bsave_Click(object sender, EventArgs e) //сохранение карты
        {
            String mapData = "";
            using (var writer = new StreamWriter(path))
            {
                foreach (var kvp in dict)
                {
                    mapData += $"{kvp.Key[0]},{kvp.Key[1]},{kvp.Value};";
                }
            }
            {
                DataBase db = new DataBase();
     
                db.setLevel(mapData);   
            }


        }
        private void Pboard_MouseUp(object sender, MouseEventArgs e) //отпускание мышки над полем
        {
            if (selected == 0 && e.Button == MouseButtons.Left) //если что-то выбрано и отпущена левая кнопка
            {
                int x, y;
                x = e.X / (pboard.Width / Cols); //поиск колонки
                y = e.Y / (pboard.Height / Rows); //поиск строки

                using (var g = Graphics.FromImage(boardBitm))
                {
                    g.Clear(DefaultBackColor);
                    g.DrawImage(img,
                            new Rectangle(xCW * x, xCH * y, xCW, xCH),
                            pics[bind[selected]], //это должны быть координаты картинки pics это координаты вырезания  bind это 2 числа и буква
                            GraphicsUnit.Pixel);
                    this.Refresh();
                    PaintEventArgs s = new PaintEventArgs(g, new Rectangle(0, 0, pboard.Width, pboard.Height));
                    Pboard_Paint(pboard, s);
                }
                int[] arr = { x, y };
                dict.Add(arr, bind[selected]);
                DrawCells();
            }
        }
        private void Pboard_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(boardBitm, 0, 0);
        }

        private void DrawCells() //отрисовка сетки
        {
            boardBitm = new Bitmap(pboard.Width, pboard.Height);
            using (var g = Graphics.FromImage(boardBitm))
            {
                g.Clear(DefaultBackColor);
                for (int i = 0; i <= Rows; i++)
                {
                    g.DrawLine(new Pen(Color.Black, 1), 0, i * xCH, Cols * xCW, i * xCH);
                }
                for (int i = 0; i <= Cols; i++)
                {
                    g.DrawLine(new Pen(Color.Black, 1), i * xCW, 0, i * xCW, Rows * xCH);
                }
                g.DrawLine(new Pen(Color.Red, 1), 0, Rows * xCH - 1, Cols * xCW, Rows * xCH - 1); //нижняя
                g.DrawLine(new Pen(Color.Red, 1), Cols * xCW - 1, 0, Cols * xCW - 1, Rows * xCH); 

                foreach (KeyValuePair<int[], char> kvp in dict)
                {
                    g.DrawImage(img,
                            new Rectangle(xCW * kvp.Key[0], xCH * kvp.Key[1], xCW, xCH),
                            pics[kvp.Value],
                            GraphicsUnit.Pixel);
                }

                pboard.Refresh();
            }
        }
        private void DictFill() //инициализация основных переменных
        {

            pboard.Height = 150; //размер поля 
            pboard.Width = 600;
            xCW = pboard.Width / Cols;
            xCH = pboard.Height / Rows;
            img = ResizeImage(img, pboard.Width, pboard.Height);
            int k = 0;
                    Rectangle tmp;
                    tmp = new Rectangle(0,0, pboard.Width, pboard.Height);
                    pics.Add(ims[k], tmp);
                    bind.Add(k, ims[k]);            
              
        }
        private Image ResizeImage(Image img, int newWidth, int newHeight) //переразмер карты
        {
            Image thumbImg = new Bitmap(newWidth, newHeight);

            using (Graphics gr = Graphics.FromImage(thumbImg))
            {
                gr.DrawImage(img, new Rectangle(0, 0, newWidth, newHeight), new Rectangle(0, 0, img.Width, img.Height), GraphicsUnit.Pixel);
            }
            return thumbImg;
        }
    }
    }
