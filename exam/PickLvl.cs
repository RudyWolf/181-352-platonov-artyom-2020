﻿using SemesterProject.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemesterProject
{
   

    public partial class PickLvl : Form
    {
        public int[,] map = new int[30, 30];
        public Image block = new Bitmap(Resources.block);
        public int sW = 10;
        public int sH = 10;
        public int num = 1;
        public int maxNum;
        public PickLvl()
        {
            InitializeComponent();
            DataBase db = new DataBase();
            String ResMap = db.getLevel(num);
            CreateMap(ResMap);
            maxNum = db.getMaxNum();
            panel1.Invalidate();
        }

        private void load() {
            DataBase db = new DataBase();
            String ResMap = db.getLevel(num);
            CreateMap(ResMap);
            maxNum = db.getMaxNum();
            panel1.Invalidate();
        }

        private void nextClick(object sender, EventArgs e)
        {
            if (num == maxNum)
            {
                num = 1;
            }
            else {
                num++;
            }
            load();
        }

        private void goClick(object sender, EventArgs e)
        {
            Form1 game = new Form1();
            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < 30; j++)
                {
                    game.resMap[i, j] = 0;
                }
            }
            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < 30; j++)
                {
                    game.resMap[i, j] = map[i, j]; 
                }
            }
            game.level = true;
            game.intLevel = num;
            game.ShowDialog();
        }


        private void CreateMap(String tmpMap)  
        {
            String[] allRowElementsInTmpMap = tmpMap.Split(';');
            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < 30; j++)
                {
                    map[i, j] = 0;
                }
            }
            for (int i = 0; i < allRowElementsInTmpMap.Length - 1; i++)
            {
                String[] allColElementsInTmpMap = allRowElementsInTmpMap[i].Split(','); 
                map[int.Parse(allColElementsInTmpMap[0]), int.Parse(allColElementsInTmpMap[1])] = int.Parse(allColElementsInTmpMap[2]);
            }
        }

        private void drawMap(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            for (int i = 0; i < 30; i++)
            {
                for (int j = 0; j < 30; j++)
                {
                    if (map[i, j] == 6) 
                    {
                        g.DrawImage(block, new Rectangle(new Point(i * sW, j * sH), new Size(sW * 1, sW * 1)), 0, 0, 165, 54, GraphicsUnit.Pixel);
                    }
                }
            }
        }
    }
}
