﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemesterProject
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }

        private void redClick(object sender, EventArgs e)
        {
            redactor rd = new redactor();
            rd.ShowDialog();
        }

        private void topClick(object sender, EventArgs e)
        {
            top tp = new top();
            tp.ShowDialog();
        }

        private void pickLvlClick(object sender, EventArgs e)
        {
            PickLvl pck = new PickLvl();
            pck.ShowDialog();
        }

        private void gameClick(object sender, EventArgs e)
        {
            Form1 fm1 = new Form1();
            fm1.ShowDialog();
        }
    }
}
