﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemesterProject
{
    public partial class top : Form
    {
        public top()
        {
            DataBase db = new DataBase();
            InitializeComponent();
            dataGridView1.Rows.Clear();
            List<string[]> top = new List<string[]>();
            top = db.top10();
            foreach (string[] rec in top)
            {
                dataGridView1.Rows.Add(rec);
            }
        }

    }
}
