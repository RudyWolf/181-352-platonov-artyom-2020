﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SemesterProject
{
    public partial class Form2 : Form
    {
        public int score;
        public int level;
        public Form2(int thisScore,int level)
        {
            InitializeComponent();
            score = thisScore;
            Result.Text = score.ToString(); 
            button1.Click += new EventHandler(addResult);
            button1.DialogResult = DialogResult.OK;
        }

        private void addResult(object sender, EventArgs e)
        {
            Name = yourName.Text;
            if (Name == "") {
                Name = "None";
            }
            DataBase db = new DataBase();
            db.setTop(score, Name, level);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            

        }
        public static rating[] Bubble_Sort(List <rating> anArray1)
        {
            rating[] anArray = anArray1.ToArray();
            
         for (int i = 0; i < anArray.Length; i++)
            {
            
                for (int j = 0; j < anArray.Length - 1 - i; j++)
                {
                    if (anArray[j].score < anArray[j + 1].score)
                    {
                        Swap(ref anArray[j], ref anArray[j + 1]);
                    }
                }
            }
            return (anArray);
        }
        public static void Swap(ref rating aFirstArg, ref rating aSecondArg)
        {
            rating tmpParam = aFirstArg;
            aFirstArg = aSecondArg;
            aSecondArg = tmpParam;
        }

    }
}
