﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SemesterProject
{
    class DataBase
    {
        public SQLiteConnection db = new SQLiteConnection("DB.db");

        
        public void setLevel (String map)
        {
            Level x = new Level();
            x.MapData = map;
            db.Insert(x);
        }

        public int getMaxNum()
        {
            List<Level> all = db.Query<Level>($"SELECT * FROM 'Level'");
            return (all.Count);
        }

        public void setTop(int score, String name, int level)
        {
            Top rec = new Top();
            rec.Score = score.ToString();
            rec.Name = name;
            rec.Level = level.ToString();
            db.Insert(rec);
        }

        public List<string[]> top10()
        {
            List<Top> rate = db.Query<Top>($"SELECT * FROM 'Top' ORDER BY Score DESC LIMIT 10");
            List<string[]> ret = new List<string[]>();

            for (int i = 0; i < rate.Count; i++)
            {
                ret.Add(new string[2]);
                ret[i][0] = rate[i].Name;
                ret[i][1] = rate[i].Score;
            }
            return (ret);


        }

        public String getLevel(int numLVL)
        {

            List<Level> map = db.Query<Level>($"SELECT * FROM 'Level' WHERE  ID ={numLVL}");
            return (map[0].MapData);
        }

        private class Level
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string MapData { get; set; }
        }
        private class Top
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }

            public string Level { get; set; }
            public string Name { get; set; }
            public string Score { get; set; }
        
        }
    }
}
