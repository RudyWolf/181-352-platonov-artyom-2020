﻿namespace SemesterProject
{
    partial class menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.freeGame = new System.Windows.Forms.Button();
            this.pickLvl = new System.Windows.Forms.Button();
            this.top = new System.Windows.Forms.Button();
            this.redactor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // freeGame
            // 
            this.freeGame.Location = new System.Drawing.Point(31, 30);
            this.freeGame.Name = "freeGame";
            this.freeGame.Size = new System.Drawing.Size(222, 200);
            this.freeGame.TabIndex = 0;
            this.freeGame.Text = "Свободная игра";
            this.freeGame.UseVisualStyleBackColor = true;
            this.freeGame.Click += new System.EventHandler(this.gameClick);
            // 
            // pickLvl
            // 
            this.pickLvl.Location = new System.Drawing.Point(289, 29);
            this.pickLvl.Name = "pickLvl";
            this.pickLvl.Size = new System.Drawing.Size(222, 200);
            this.pickLvl.TabIndex = 1;
            this.pickLvl.Text = "Выбрать уровень";
            this.pickLvl.UseVisualStyleBackColor = true;
            this.pickLvl.Click += new System.EventHandler(this.pickLvlClick);
            // 
            // top
            // 
            this.top.Location = new System.Drawing.Point(537, 30);
            this.top.Name = "top";
            this.top.Size = new System.Drawing.Size(222, 200);
            this.top.TabIndex = 2;
            this.top.Text = "Топ игроков";
            this.top.UseVisualStyleBackColor = true;
            this.top.Click += new System.EventHandler(this.topClick);
            // 
            // redactor
            // 
            this.redactor.Location = new System.Drawing.Point(796, 30);
            this.redactor.Name = "redactor";
            this.redactor.Size = new System.Drawing.Size(222, 200);
            this.redactor.TabIndex = 3;
            this.redactor.Text = "Редактор карт";
            this.redactor.UseVisualStyleBackColor = true;
            this.redactor.Click += new System.EventHandler(this.redClick);
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 259);
            this.Controls.Add(this.redactor);
            this.Controls.Add(this.top);
            this.Controls.Add(this.pickLvl);
            this.Controls.Add(this.freeGame);
            this.Name = "menu";
            this.Text = "menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button freeGame;
        private System.Windows.Forms.Button pickLvl;
        private System.Windows.Forms.Button top;
        private System.Windows.Forms.Button redactor;
    }
}